const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// 1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
// 3. Process a POST request at the /details route using postman to retrieve the details of the user.

// auth.verify method acts as a middleware to ensure that the user is logged in before they can enroll a course
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode (req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course

/*1. Refactor the user route to implement user authentication for the enroll route.
2. Process a POST request at the /enroll route using postman to enroll a user to a course.
*/
router.post("/enroll/:userId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	let data = {
		courseId : req.body.courseId
	};

	userController.enroll(user, data, req.params).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in "index.js"
module.exports = router;